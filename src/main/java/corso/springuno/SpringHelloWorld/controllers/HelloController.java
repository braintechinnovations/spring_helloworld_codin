package corso.springuno.SpringHelloWorld.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import corso.springuno.SpringHelloWorld.models.Persona;

@RestController
@RequestMapping("/hello")
public class HelloController {

	@GetMapping("/giovanni")
	public String salutaGiovanni() {
		return "Giovanni";
	}
	
	@GetMapping("/mario")
	public String salutaMario() {
		System.out.println("Sono nella rotta di Mario");
		
		return this.salutaGiovanni();
	}

	@GetMapping("/esempio")
	public void esempio() {
		
	}
	
//	@GetMapping("/saluta/{nome}")
//	public String salutaGenerico(@PathVariable String nome) {
//		return "Ciao " + nome;
//	}
	
	@GetMapping("/saluta/{var_nome}")
	public String salutaGenerico(@PathVariable(name = "var_nome") String nome) {
		return "Ciao " + nome;
	}
	
	//	http://localhost:8080/hello/persona?varNome=Giovanni&varCogn=Pace
//	@GetMapping("/persona")
//	public String salutaPersona(
//			@RequestParam(name = "varNome") String nome, 
//			@RequestParam(name = "varCogn") String cogn) {
//		return "Ciao " + nome + " " + cogn;
//	}
	
	@GetMapping("/persona")
	public Persona salutaPersona(
			@RequestParam(name = "varNome") String nome, 
			@RequestParam(name = "varCogn") String cogn) {

		Persona temp = new Persona(nome, cogn);
		System.out.println(temp);
		
		
		return temp;
	}
	
	@PostMapping("/test")
	public String testPost() {
		return "sono POST :)";
	}
	
	@GetMapping("/test")
	public String testGet() {
		return "sono GET :)";
	}
	
	@RequestMapping("/generico")
	public String genericoMapping() {
		return "sono un metodo Generico";
	}
}
