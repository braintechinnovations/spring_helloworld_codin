package corso.springuno.SpringHelloWorld.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import corso.springuno.SpringHelloWorld.models.Persona;

@RestController
@RequestMapping("/persona")
public class PersonaController {

	@PostMapping("/inserisci")
	public Persona inserisciPersona(@RequestBody Persona objPer) {
		return objPer;
	}
	
	@GetMapping("/lista")
	public List<Persona> restituisciPersone(){
		
		Persona perUno = new Persona("Giovanni", "Pace");
		Persona perDue = new Persona("Mario", "Rossi");
		Persona perTre = new Persona("Valeria", "Verdi");
		
		List<Persona> elenco = new ArrayList<Persona>();

		elenco.add(perUno);
		elenco.add(perDue);
		elenco.add(perTre);
		
		return elenco;
	}
	
	@PostMapping("/importa")
	public List<Persona> importaPersone(@RequestBody List<Persona> objElenco){
		
		for(int i=0; i<objElenco.size(); i++) {
			Persona temp = objElenco.get(i);
			System.out.println(temp);
		}
		
		return objElenco;
	}
}
